package pingpong

import (
	"math/rand"
	"os"
	"time"
)

const MaxMagic = 1 << 16

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Ping struct {
	Ping bool
}

func NewPing() Ping {
	return Ping{true}
}

type Pong struct {
	Pong        bool
	Origin      string
	MagicNumber int
}

func NewPong(Ping) Pong {
	origin, _ := os.LookupEnv("HOSTNAME")
	return Pong{true, origin, rand.Intn(MaxMagic)}
}
