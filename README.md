# ping-pong

_Ping pong microservices._

## Get Started

1. Log in to your OpenShift cluster via `oc login`
1. Provide the image pull secret for the external registry: `oc create secret docker-registry gitlab-deploy-token --docker-server=registry.gitlab.com --docker-username=gitlab+deploy-token-XXXXXX --docker-password=YYYYYYYYYYYYYYYY`
1. Link the pull secret to the default service account: `oc secrets link default gitlab-deploy-token --for=pull`
1. Apply all manifests: `oc apply $(find deploy/*.yml -exec echo -n ' -f {}' \;)`
