package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	pingpong "gitlab.com/evanstoner/ping-pong/pkg"
)

func ping(pongAddr string) (pong pingpong.Pong) {
	resp, err := http.Get(fmt.Sprintf("http://%s/ping", pongAddr))
	if err != nil {
		log.Fatalln(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(body, &pong)
	if err != nil {
		log.Fatalln(err)
	}

	return
}

func main() {
	pongAddr, ok := os.LookupEnv("PONG_ADDR")
	if !ok {
		log.Println("hint: you can use PONG_ADDR to override the pong server address")
		pongAddr = "localhost:8001"
	}

	pingIntervalStr, ok := os.LookupEnv("PING_INTERVAL")
	if !ok {
		log.Println("hint: you can use PING_INTERVAL to override the ping interval in milliseconds")
		pingIntervalStr = "2000"
	}
	pingInterval, err := strconv.Atoi(pingIntervalStr)
	if err != nil {
		panic(err)
	}

	log.Printf("pongAddr     => %s\n", pongAddr)
	log.Printf("pingInterval => %d\n", pingInterval)

	ticker := time.NewTicker(time.Duration(pingInterval * int(time.Millisecond)))

	for {
		pong := ping(pongAddr)
		log.Printf("got pong back from '%s' with magic number => 0x%04X\n", pong.Origin, pong.MagicNumber)
		<-ticker.C
	}
}
