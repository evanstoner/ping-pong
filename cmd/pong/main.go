package main

import (
	"encoding/json"
	"log"
	"net/http"

	pingpong "gitlab.com/evanstoner/ping-pong/pkg"
)

func ping(w http.ResponseWriter, req *http.Request) {
	b, err := json.Marshal(pingpong.NewPong(pingpong.NewPing()))
	if err == nil {
		log.Println("got ping")
		w.Write(b)
	} else {
		go http.Error(w, err.Error(), 500)
	}
}

func main() {
	log.Println("pong starting up...")

	http.HandleFunc("/ping", ping)

	http.ListenAndServe(":8001", nil)
}
